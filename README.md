Bymorev Helpers
===============
Bymorev Helpers

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist bymorev-helpers/yii2-bymorev-helpers "*"
```

or add

```
"bymorev-helpers/yii2-bymorev-helpers": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \Bymorev\AutoloadExample::widget(); ?>```