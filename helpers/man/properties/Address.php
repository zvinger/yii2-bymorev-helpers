<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 12.03.16
 * Time: 14:20
 */

namespace Bymorev\helpers\man\properties;

use Bymorev\helpers\man\Man;
use yii\base\Model;

trait Address
{
	public static function toString(Man $man)
	{
		$fields = [
			["zipcode", ','],
			["country", ','],
			["region", ','],
			["city", ','],
			["street", ','],
			["house", ','],
			["apartment", ','],
		];
		$str = NULL;
		foreach ($fields as $field) {
			if (isset($man->{$field[0]}) && $man->{$field[0]}) {
				$str .= $man->{$field[0]};
				if (isset($field[1])) {
					$str .= $field[1];
				}
				$str .= ' ';
			}
		}
		return $str;
	}

	/**
	 * @param \yii\widgets\ActiveForm $form
	 * @return string
	 */
	public function address_form($form)
	{
		/** @var Model $this */
		$ret =
			'<div class=row>' .
			'<div class="col-xs-12 col-lg-4">'
			.
			$form->field($this, 'zipcode')->textInput(['maxlength' => TRUE])
			.
			'</div>'

			.
			'<div class="col-xs-12 col-lg-4">'
			.
			$form->field($this, 'country')->textInput(['maxlength' => TRUE])
			.
			'</div>'
			.
			'<div class="col-xs-12 col-lg-4">'
			.
			$form->field($this, 'region')->textInput(['maxlength' => TRUE])
			.
			'</div>' .
			'</div>'
			.
			'<div class=row>' .
			'<div class="col-xs-12 col-lg-4">'
			.
			$form->field($this, 'city')->textInput(['maxlength' => TRUE])
			.
			'</div>'
			.
			'<div class="col-xs-12 col-lg-4">'
			.
			$form->field($this, 'house')->textInput(['maxlength' => TRUE])
			.
			'</div>'
			.
			'<div class="col-xs-12 col-lg-4">'
			.
			$form->field($this, 'apartment')->textInput(['maxlength' => TRUE])
			.
			'</div>'
			.
			'</div>';

		return $ret;
	}
}