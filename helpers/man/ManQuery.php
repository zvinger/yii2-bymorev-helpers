<?php

namespace Bymorev\helpers\man;

/**
 * This is the ActiveQuery class for [[Man]].
 *
 * @see Man
 */
class ManQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Man[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
    public function fio($fio)
    {

    }
    /**
     * @inheritdoc
     * @return Man|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}