<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 12.10.17
 * Time: 13:56
 */

namespace Bymorev\helpers\traits\php;

trait LikeSingletonInstanceAbsctractClass
{
    final public static function instance($config = [])
    {
        static $instances = [];

        $calledClass = get_called_class();

        if (!isset($instances[$calledClass])) {
            $instances[$calledClass] = new $calledClass($config);
        }

        return $instances[$calledClass];
    }
}