<?php
namespace Bymorev\helpers\traits;

use yii\helpers\ArrayHelper;

/**
 * Class Collector
 * @package Bymorev\helpers\traits
 */
trait Collector
{
    private static $_cacheKeyPrefix = "SelectorArrayCached__";

    protected static $_current_class_objects_array = FALSE;

    /**
     * @param string $field Название поля, которое поставлять в значение
     * @return array|bool
     */
    public static function SelectArray($field = 'title')
    {
        $currentCacheKey = static::$_cacheKeyPrefix . self::classname();
        if (static::$_current_class_objects_array === FALSE) {
            try {
                static::$_current_class_objects_array = \Yii::$app->cache->get($currentCacheKey);
                if (!static::$_current_class_objects_array) {
                    static::$_current_class_objects_array = ArrayHelper::map(static::find()->all(), 'id', $field);
                    \Yii::$app->cache->set($currentCacheKey, static::$_current_class_objects_array, 10);
                }
            } catch (\Exception $e) {
                static::$_current_class_objects_array = ArrayHelper::map(static::find()->all(), 'id', $field);
            }
        }

        return static::$_current_class_objects_array;
    }

    protected static $_current_class_all_objects = FALSE;

	/**
     * @return static[]
     */
	public static function getAll()
    {
        if (static::$_current_class_all_objects === FALSE) {
            static::$_current_class_all_objects = static::find()->all();
        }

        return static::$_current_class_all_objects;
    }
}