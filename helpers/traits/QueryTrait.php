<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 05.06.16
 * Time: 12:15
 */

namespace Bymorev\helpers\traits;

use yii\db\ActiveQuery;

trait QueryTrait
{
	/**
	 * @param int|null $user_id
	 * @return $this
	 */
	public function available($user_id = NULL)
	{
		/** @var ActiveQuery $this */
		if (!$user_id) {
			$user_id = \Yii::$app->user->id;
		}
		$isDirector = \Yii::$app->authManager->checkAccess($user_id, "director");
		if (!$isDirector) {
			$this->andWhere(['user_id' => $user_id]);
		}

		return $this;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function by_id($id)
	{
		/** @var ActiveQuery $this */
		return $this->andWhere(['id'=>$id]);
	}
}