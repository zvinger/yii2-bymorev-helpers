<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 21.08.16
 * Time: 22:09
 */

namespace Bymorev\helpers\traits;

use Yii;

trait CachedDataTrait
{
    protected $_cached_data = [];

    protected function getCachedData($key = NULL)
    {
        if (!isset($this->_cached_data_key)) {
            throw new \Exception("No cached data key set");
        }
        if (!$key) {
            return NULL;
        }
        $final_key = $this->makeFinalKey($key);
        if ($final_key && isset($this->_cached_data[$final_key])) {
            return $this->_cached_data[$final_key];
        }
        $cached_data = Yii::$app->cache->get($final_key);
        if ($cached_data) {
            return $cached_data;
        }

        return NULL;
    }

    protected function setCachedData($key, $value)
    {
        if (!$value) {
            return;
        }
        $finalKey = $this->makeFinalKey($key);
        $this->_cached_data[$finalKey] = $value;
        if ($finalKey) {
            Yii::$app->cache->set($finalKey, $value, 300);
        }
    }

    private function makeFinalKey($key)
    {
        if (isset($this->id)) {
            $finalKey = $this->_cached_data_key . '_' . $key . '_' . $this->id;
            return $finalKey;
        } else {
            return FALSE;
        }
    }

    protected function flushKey($key)
    {
        $finalKey = $this->makeFinalKey($key);
        if ($finalKey) {
            Yii::$app->cache->set($finalKey, null);
            Yii::$app->cache->delete($finalKey);
        }
    }
}