<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 11.11.16
 * Time: 19:03
 */

namespace Bymorev\helpers\traits;

use yii\log\Logger;

trait LoggerTrait
{
    protected static $_log_category = NULL;

    public static function Log($message, $level = Logger::LEVEL_INFO, $category = FALSE)
    {
        if (!$category) {
            $category = !empty(self::$_log_category) ? self::$_log_category : NULL;
        }
        \Yii::getLogger()->log($message, $level, $category);
        YII_DEBUG ? \Yii::getLogger()->flush() : NULL;
    }
}