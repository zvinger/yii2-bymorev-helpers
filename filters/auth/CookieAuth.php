<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 12.07.16
 * Time: 17:22
 */

namespace Bymorev\filters\auth;

use yii\filters\auth\AuthMethod;

class CookieAuth extends AuthMethod
{
    public function authenticate($user, $request, $response)
    {
        if ($user->id && $user->identity) {
            return $user;
        } else {
            return NULL;
        }
    }
}