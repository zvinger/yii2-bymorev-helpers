<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 16.09.16
 * Time: 10:32
 */

namespace Bymorev\components\telegram\auth;

use Bymorev\components\telegram\models\TelegramUserConnection;
use Yii;

class TelegramAuth
{
    protected $_user_id;

    protected $_telegram_connection;
    protected $_telegram_user_id;

    public function __construct($user_id)
    {
        $this->_user_id = $user_id;
    }

    protected function generateKeyForUser()
    {
        $security = Yii::$app->security;
        $string = $security->generateRandomString(32);

        return $security->generatePasswordHash($this->_user_id . $string);
    }

    public function getUserConnection()
    {
        if (!$model = $this->getTelegramUserConnection()) {
            $model = new TelegramUserConnection();
            $model->user_id = $this->_user_id;
            $model->auth_hash = $this->generateKeyForUser();
            $model->save();
        }

        return $model;
    }

    public function isUserAuth()
    {
        $telegramUserConnection = $this->getTelegramUserConnection();

        return ($telegramUserConnection && $telegramUserConnection->telegram_chat_id) ? TRUE : FALSE;
    }

    protected function getTelegramUserConnection()
    {
        if (!$this->_telegram_connection) {
            $this->_telegram_connection = TelegramUserConnection::find()->byUserId($this->_user_id)->one();
        }

        return $this->_telegram_connection;
    }

    public function checkUpdates()
    {
        $updates = Yii::$app->telegram->getUpdates();
        if ($this->isUserAuth()) {
            return TRUE;
        }
        $connection = $this->getTelegramUserConnection();
        if (!$connection) {
            return FALSE;
        }
        $hash = $connection->auth_hash;
        foreach ($updates as $update) {
            /** @var \Telegram\Bot\Objects\Update $update */
            $message = $update->getMessage();
            if ($hash == $message->getText()) {
                $telegram_chat_id = $message->getFrom()->get('id');
                $connection->telegram_chat_id = $telegram_chat_id;
                $connection->save(FALSE, ['telegram_chat_id']);
                $this->_telegram_connection = $connection;
                Yii::$app->telegram->message($telegram_chat_id, "Благодарим за авторизацию на " . Yii::$app->name);

                return TRUE;
            }
        }
    }

    public function getUserTelegramId()
    {
        if (!$this->_telegram_user_id) {
            $connection = $this->getTelegramUserConnection();

            $this->_telegram_user_id = isset($connection->telegram_chat_id) ? $connection->telegram_chat_id : NULL;
        }

        return $this->_telegram_user_id;
    }
}