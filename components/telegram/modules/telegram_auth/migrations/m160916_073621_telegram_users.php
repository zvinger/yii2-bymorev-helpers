<?php
namespace Bymorev\components\telegram\modules\telegram_auth\migrations;

use yii\db\Schema;
use yii\db\Migration;

class m160916_073621_telegram_users extends Migration
{
    /*
    public $table_for_add_columns = ["mm_company"];
    public $add_column_names = ["name2"=>["string(200)"],];
    */
    public function safeUp()
    {
        $tableOptions = NULL;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tools_telegram_user_connection}}', [
            'id'               => $this->primaryKey(),
            'user_id'          => $this->integer(),
            'telegram_chat_id' => $this->string(500) . " COMMENT 'ID телеграм'",
            'auth_hash'        => $this->string(500) . " COMMENT 'Соль авторизации'",

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('FK_TOOLS_TELEGRAM_USER_ID', '{{%tools_telegram_user_connection}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->execute("CREATE UNIQUE INDEX tools_telegram_user_connection_user_id_uindex ON tools_telegram_user_connection (user_id);");
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_TOOLS_TELEGRAM_USER_ID', '{{%tools_telegram_user_connection}}');
        $this->dropTable('tools_telegram_user_connection');
    }
}
