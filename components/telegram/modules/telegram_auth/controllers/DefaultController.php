<?php

namespace Bymorev\components\telegram\modules\telegram_auth\controllers;

use Bymorev\components\telegram\auth\TelegramAuth;
use common\models\User;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `telegram_auth_module` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAuth($check_updates = FALSE)
    {
        $user = Yii::$app->user;
        $telegramAuth = new TelegramAuth($user->id);
        $params = [
            'user'         => $user,
            'telegramAuth' => $telegramAuth,
        ];
        if ($check_updates) {
            $telegramAuth->checkUpdates();
        }

        return $this->render('auth/auth', $params);
    }

    public function actionKickChatMember($user_id = 0, $chat_id = 0)
    {
        /*
         * Поиск пользователей по роли
         */
//        $users = User::find()->byRole("manager")->all();
//        $telegram = Yii::$app->telegram;
//        $telegram->message($telegram->getTelegramMe());
        /*
         * Метод удаления человека из группы
        Yii::$app->telegram->getTelegram()->kickChatMember([
            'chat_id' => -1001072179810,
            'user_id' => 155416804,
        ]);
        */
    }
}
