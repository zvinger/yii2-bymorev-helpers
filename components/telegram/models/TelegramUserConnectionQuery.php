<?php

namespace Bymorev\components\telegram\models;

/**
 * This is the ActiveQuery class for [[TelegramUserConnection]].
 *
 * @see TelegramUserConnection
 */
class TelegramUserConnectionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TelegramUserConnection[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function byUserId($user_id)
    {
        return $this->andWhere(['user_id'=>$user_id]);
    }

    /**
     * @inheritdoc
     * @return TelegramUserConnection|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
