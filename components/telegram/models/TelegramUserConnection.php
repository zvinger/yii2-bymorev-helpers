<?php

namespace Bymorev\components\telegram\models;

use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tools_telegram_user_connection".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $telegram_chat_id
 * @property string $auth_hash
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class TelegramUserConnection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tools_telegram_user_connection';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['telegram_chat_id', 'auth_hash'], 'string', 'max' => 500],
            [['user_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'user_id'          => 'User ID',
            'telegram_chat_id' => 'ID телеграм',
            'auth_hash'        => 'ID телеграм',
            'created_at'       => 'Created At',
            'updated_at'       => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return TelegramUserConnectionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TelegramUserConnectionQuery(get_called_class());
    }
}
