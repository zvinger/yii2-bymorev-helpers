<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 06.06.16
 * Time: 17:16
 */

namespace Bymorev\components\gearman\helpers;

class JobNames
{
    public static $jobPrefix = NULL;

    public static function make($name, $worker_id = NULL)
    {
        if (empty(self::$jobPrefix)) {
            self::$jobPrefix = \Yii::getAlias("@frontendUrl");
            if (empty(self::$jobPrefix)) {
                self::$jobPrefix = 'GearmanTask';
            }
        }

        return \Yii::getAlias("@frontendUrl") . '::' . $worker_id . '::' . $name;
    }
}